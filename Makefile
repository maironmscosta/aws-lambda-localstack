
zip-file:
	zip function.zip lambda_function.py script/ -r --exclude=.aws-sam --exclude=*.pyc --exclude=*/__pycache__/*

deploy-zip:
	$(MAKE) zip-file
	aws lambda update-function-code --function-name aws-lambda-localstack --zip-file fileb://function.zip